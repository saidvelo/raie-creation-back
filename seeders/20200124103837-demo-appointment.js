'use strict';

const faker = require("faker");

module.exports = {
  up: (queryInterface, Sequelize) => {
    const fakeList = [];
    const users = [1, 2, 3, 4, 5, 6 ,7 ,8, 9, 10, 1, 4, 5, 7, 8]
    for (let i = 0; i < 15; i++) {
      const fakeObj = {
        userId: users[i],
        salonId: faker.random.number({min:1, max:3}),
        date: faker.date.future(1),
        isUserOk: faker.random.boolean(),
        isSalonOk: faker.random.boolean(),
        createdAt: faker.date.past(1),
        updatedAt: faker.date.recent(30)
      };
      fakeList.push(fakeObj);
    }

    return queryInterface.bulkInsert('Appointments', fakeList, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Appointments', null, {});
  }
};
