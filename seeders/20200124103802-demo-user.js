'use strict';

const faker = require("faker");

module.exports = {
  up: (queryInterface, Sequelize) => {
    const fakeUsers = [];

    for (let i = 0; i < 10; i++) {
      const firstName = faker.name.firstName();
      const lastName = faker.name.lastName();
      const fakeUser = {
        firstName: firstName,
        lastName: lastName,
        email: firstName.toLowerCase() + '.' + lastName.toLowerCase() + '@gmail.com',
        password: faker.internet.password(),
        phone: faker.phone.phoneNumber(),
        createdAt: faker.date.past(1),
        updatedAt: faker.date.past(1)
      };
      fakeUsers.push(fakeUser);
    }

    return queryInterface.bulkInsert('Users', fakeUsers, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};