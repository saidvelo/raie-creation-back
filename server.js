const express = require("express"),
app = express(),
bodyParser = require("body-parser");

const port = process.env.PORT || 3000;
const cors = require("cors");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

require('./routes/userCtrl')(app);
require('./routes/salonCtrl')(app);
require('./routes/appointmentCtrl')(app);
require('./routes/imageCtrl')(app);

app.listen(port);

console.log("API server started on: " + port);