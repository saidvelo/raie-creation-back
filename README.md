# raie-creation-back

TP Fullstack

# Install dependencies

npm i

# Create the database and call it :
'raiecreation_dev'

# Configure it at :
config/config.json

# This command will create tables
npx sequelize-cli db:migrate

# This command will insert fake data
npx sequelize-cli db:seed:all


# If you want to update the DB drop it with :
npx sequelize-cli db:migrate:undo:all
npx sequelize-cli db:migrate
npx sequelize-cli db:seed:all
