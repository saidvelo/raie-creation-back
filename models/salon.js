'use strict';
module.exports = (sequelize, DataTypes) => {
  const Salon = sequelize.define('Salon', {
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    adress: DataTypes.STRING
  }, {});
  Salon.associate = function(models) {
    Salon.belongsToMany(models.User, {through: 'Appointment', as: 'clients', foreignKey: 'salonId'});
    Salon.hasMany(models.Image, {foreignKey: 'salonId'})
  };
  return Salon;
};