const models = require('../models');
const asyncHandler = require('express-async-handler');


module.exports = function(router) {
    
    router.post("/appointments", (req, res) => {
        models.Appointment.create({ 
            userId: req.body.userId,
            salonId: req.body.salonId,
            date: req.body.date,
            isUserOk: req.body.isUserOk,
            isSalonOk: req.body.isSalonOk
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
    });
  
    router.put("/appointments/user/:userId/salon/:salonId", (req, res) => {
        models.Appointment.update({ 
            userId: req.body.userId,
            salonId: req.body.salonId,
            date: req.body.date,
            isUserOk: req.body.isUserOk,
            isSalonOk: req.body.isSalonOk,
            updatedAt: new Date,
        }, { 
            where: { 
                userId: req.params.userId,
                salonId: req.params.salonId,
            } 
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
      });
    
    router.delete("/appointments/user/:userId/salon/:salonId", (req, res) => {
    models.Appointment.destroy({
        where: { 
            userId: req.params.userId,
            salonId: req.params.salonId
        }
    })
        .then(result => {
        res.json(result);
        })
        .catch(err => {
        res.json(err);
        console.log('err' + err);
        });
    });
}