const models = require('../models');
const asyncHandler = require('express-async-handler');


module.exports = function(router) {
    router.get("/users", (req, res) => {
      models.User.findAll()
        .then(result => {
          res.json(result);
        })
        .catch(err => {
          res.json(err);
          console.log('err' + err);
        });
    });

    router.get("/users/:id", (req, res) => {
        models.User.findByPk(req.params.id, {
          include: [
            {
              model: models.Salon,
              as: 'appointments',
              attributes: ['id', 'adress', 'city', 'country'],
              through: {
                model: models.Appointment,
                attributes: ['date', 'isUserOk', 'isSalonOk']
              }
            }
          ]
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
            console.log('err' + err);
          });
      });

      router.post("/users", (req, res) => {
        models.User.create({ 
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: req.body.password,
          phone: req.body.phone,
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
      });
  
    router.put("/users/:id", (req, res) => {
        models.User.update({ 
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: req.body.password,
          phone: req.body.phone,
        }, { where: { id: req.params.id } })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
    });
    
    router.delete("/users/:id", (req, res) => {
    models.User.destroy({
        where: { id: req.params.id }
    })
        .then(result => {
        res.json(result);
        })
        .catch(err => {
        res.json(err);
        console.log('err' + err);
        });
    });

    router.post("/users/appointment/:id", (req, res) => {
        models.Appointment.create({ 
          userId: req.params.id,
          salonId: req.body.salonId,
          date: req.body.date,
          isUserOk: true,
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
    });

    router.put("/users/:userid/appointment/:salonId", (req, res) => {
        models.Appointment.update({ 
            userId: req.params.id,
            salonId: req.body.salonId,
            date: req.body.date,
            isUserOk: req.body.isUserOk,
        }, {where: {
            [Op.or]: [{userId: req.params.userId}, {salonId: req.params.salonId}]
            }
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
    });
}