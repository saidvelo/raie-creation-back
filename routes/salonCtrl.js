const models = require('../models');
const asyncHandler = require('express-async-handler');


module.exports = function(router) {
    router.get("/salons", (req, res) => {
      models.Salon.findAll()
        .then(result => {
          res.json(result);
        })
        .catch(err => {
          res.json(err);
          console.log('err' + err);
        });
    });

    router.get("/salons/:id", (req, res) => {
        models.Salon.findByPk(req.params.id, {
          include: [
              {
                model: models.Image,
                attributes: ['id', 'url', 'legend', 'createdAt']
              },
            {
                model: models.User,
                as: 'clients',
                attributes: ['id', 'firstName', 'lastName', 'email', 'phone'],
                through: {
                    model: models.Appointment,
                    attributes: ['date']
                }
            }
          ]
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
                console.log('err' + err);
            });
      });

      router.post("/salons", (req, res) => {
        models.Salon.create({ 
            country: req.body.country,
            city: req.body.city,
            adress: req.body.adress
        })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
      });
  
    router.put("/salons/:id", (req, res) => {
        models.Salon.update({ 
            country: req.body.country,
            city: req.body.city,
            adress: req.body.adress
        }, { where: { id: req.params.id } })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
      });
    
      router.delete("/salons/:id", (req, res) => {
        models.Image.destroy({
            where: {salonId: req.params.id}
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
                console.log('err' + err);
            });

        models.Salon.destroy({
            where: { id: req.params.id }
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
                console.log('err' + err);
            });
      });
}