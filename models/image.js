'use strict';
module.exports = (sequelize, DataTypes) => {
  const Image = sequelize.define('Image', {
    url: DataTypes.STRING,
    legend: DataTypes.STRING,
    salonId: DataTypes.INTEGER
  }, {});
  Image.associate = function(models) {
    Image.belongsTo(models.Salon, {foreignKey: 'salonId'})
  };
  return Image;
};