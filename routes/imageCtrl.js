const models = require('../models');
const asyncHandler = require('express-async-handler');


module.exports = function(router) {
    router.get("/images", (req, res) => {
      models.Image.findAll()
        .then(result => {
          res.json(result);
        })
        .catch(err => {
          res.json(err);
          console.log('err' + err);
        });
    });

    router.get("/images/:id", (req, res) => {
        models.Image.findByPk(req.params.id, {
          include: [
            {
              model: models.User,
              as: 'clients',
              attributes: ['id', 'firstName', 'lastName', 'email', 'phone'],
              through: {
                model: models.Appointment,
                attributes: ['date']
              }
            }
          ]
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
                console.log('err' + err);
            });
      });

      router.post("/images/:salonId", (req, res) => {
        models.Image.create({ 
            url: req.body.url,
            legend: req.body.legend,
            salonId: req.params.salonId
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
            });
      });
  
    router.put("/images/:id", (req, res) => {
        models.Image.update({ 
            url: req.body.url,
            legend: req.body.legend,
            salonId: req.body.salonId
        }, { where: { id: req.params.id } })
          .then(result => {
            res.json(result);
          })
          .catch(err => {
            res.json(err);
          });
          
      });
    
      router.delete("/images/:id", (req, res) => {
        models.Image.destroy({
            where: { id: req.params.id }
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
                console.log('err' + err);
            });
      });
}