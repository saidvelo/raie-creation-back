'use strict';

const faker = require("faker");

module.exports = {
  up: (queryInterface, Sequelize) => {
    const fakeJobs = [];

    for (let i = 0; i < 5; i++) {
      const fakeJob = {
        url: faker.internet.url(),
        legend: faker.lorem.words(5),
        salonId: faker.random.number({min: 1, max: 3}),
        createdAt: faker.date.past(1),
        updatedAt: faker.date.recent(30)
      };
      fakeJobs.push(fakeJob);
    }

    return queryInterface.bulkInsert('Images', fakeJobs, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Images', null, {});
  }
}