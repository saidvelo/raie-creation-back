'use strict';
module.exports = (sequelize, DataTypes) => {
  const Appointment = sequelize.define('Appointment', {
    userId: DataTypes.INTEGER,
    salonId: DataTypes.INTEGER,
    date: DataTypes.DATE,
    isUserOk: DataTypes.BOOLEAN,
    isSalonOk: DataTypes.BOOLEAN
  }, {});
  Appointment.associate = function(models) {
    Appointment.belongsTo(models.User, {foreignKey: 'userId'});
    Appointment.belongsTo(models.Salon, {foreignKey: 'salonId'})
  };
  return Appointment;
};