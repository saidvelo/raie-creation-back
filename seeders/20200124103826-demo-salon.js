'use strict';

const faker = require("faker");

module.exports = {
  up: (queryInterface, Sequelize) => {
    const fakeList = [];
    for (let i = 0; i < 3; i++) {
      const fakeObj = {
        country: faker.address.county(),
        city: faker.address.city(),
        adress: faker.address.streetAddress() + faker.address.streetName(),
        createdAt: faker.date.past(1),
        updatedAt: faker.date.past(1)
      };
      fakeList.push(fakeObj);
    }

    return queryInterface.bulkInsert('Salons', fakeList, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Salons', null, {});
  }
}